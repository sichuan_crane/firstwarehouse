#include<iostream>
using namespace std;
int main()
{ 
  int n,star=0,end=0;
  //实现整型数组的输入
  cout<<"输入整型数组的长度";
  cin>>n;
  int *p=new int[n];
  cout<<"输入一个整型数组";
  for(int i=0;i<n;i++)
  { 
      cin>>p[i];
  }
  int sum=p[0];
  int max_sum=p[0];
  for(int i=1;i<n;i++)
  {   
      if(sum>0)    //从第二个数开始判断是否为正数
      { 
          sum=sum+p[i];  //是正数，将和值（初始值为p【0】）和第i+1个数做和。
         if(sum>max_sum) // 并判断是否为最大，是让其赋值给max_sum，并将子数组末位置改变。
           { 
               max_sum=sum;
               end=i;
           }
      }
      else 
      { 
          sum=p[i];  //是负数，将第i+1个数的值赋值给和值sum
             if(sum>max_sum)//并判断是否为最大，是让其赋值给max_sum，并将子数组初、末位置改变。
             {    
                max_sum=sum;
                star=i;
                end=i;
             }
      }
  }
  cout<<"最大子数组位置为第"<<star+1<<"个到第"<<end+1<<"个"<<endl;
  cout<<"最大子数组和为"<<max_sum<<endl;
  system("pause");
  return 0;
}